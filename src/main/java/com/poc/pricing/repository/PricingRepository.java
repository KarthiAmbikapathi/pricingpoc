package com.poc.pricing.repository;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PricingRepository extends JpaRepository<PricingDO, Long>{

}
