package com.poc.pricing.exception;

public class PricingException extends Exception {

	public PricingException(String message) {
		super(message);
	}

}
